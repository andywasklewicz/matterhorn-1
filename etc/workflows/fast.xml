<?xml version="1.0" encoding="UTF-8"?>
<definition xmlns="http://workflow.opencastproject.org">

  <id>fast</id>
  <title>Fast Testing Workflow</title>
  <tags>
    <tag>upload</tag>
    <tag>schedule</tag>
    <tag>archive</tag>
  </tags>
  <description>
    A minimal workflow that transcodes the media into distribution formats, then
    sends the resulting distribution files, along with their associated metadata,
    to the distribution channels.
  </description>

  <configuration_panel>
  <![CDATA[
    <fieldset>
      <legend>Holds</legend>
      <span>Processing should be paused to allow for:</span>
      <ul class="oc-ui-checkbox-list-upload">
        <li class="ui-helper-clearfix">
          <span id="trimholdconfig">
            <input id="trimHold" name="trimHold" type="checkbox" class="configField holdCheckbox" value="true" />
            <span id="i18n_hold_for_trim">&nbsp;Review / Trim before encoding (with option to edit info)</span>
          </span>
        </li>
      </ul>
    </fieldset>
    <fieldset>
      <legend>Archive</legend>
      <ul class="oc-ui-checkbox-list-upload">
        <li class="ui-helper-clearfix">
          <span id="archiveconfig">
            <input id="archiveOp" name="archiveOp" type="checkbox" checked="checked" class="configField holdCheckbox" value="true"/>
            <span id="i18n_activate_archive">&nbsp;Archive the media package</span>
          </span>
        </li>
      </ul>
    </fieldset>  
    <fieldset>
      <legend>Distribution</legend>
      <ul class="oc-ui-form-list">
        <!-- field: License -->
        <li class="ui-helper-clearfix">
          <label class="scheduler-label"><span class="color-red">* </span><span id="i18n_dist_label">Distribution Channel(s)</span>:</label>
          <span id="dist">
            <input id="distribution" name="distribution" type="checkbox" checked="checked" disabled="disabled" class="configField" value="Matterhorn Media Module" />
            <span id="i18n_dist_mmm">&nbsp;Matterhorn Media Module</span>
          </span>
        </li>
      </ul>
    </fieldset>
    <script type="text/javascript">
      var ocWorkflowPanel = ocWorkflowPanel || {};

      var trimHold = $('input#trimHold');
      
      ocWorkflowPanel.registerComponents = function(components){
        /* components with keys that begin with 'org.opencastproject.workflow.config' will be passed
         * into the workflow. The component's nodeKey must match the components array key.
         *
         * Example:'org.opencastproject.workflow.config.myProperty' will be availible at ${my.property}
         */
        components['org.opencastproject.workflow.config.trimHold'] = new ocAdmin.Component(
          ['trimHold'],
          {key: 'org.opencastproject.workflow.config.trimHold'},
          {getValue: function(){ return this.fields.trimHold.is(":checked"); }
          });
          
        components['org.opencastproject.workflow.config.archiveOp'] = new ocAdmin.Component(
          ['archiveOp'],
          {key: 'org.opencastproject.workflow.config.archiveOp'},
          {getValue: function(){ return this.fields.archiveOp.is(":checked");}
          });
          
        ocScheduler.dublinCore.components['license'] = new ocAdmin.Component(
          ['license'],
          {label: 'licenseLabel', key: 'license'}
          );
          
          //etc...
      }
      ocWorkflowPanel.setComponentValues = function(values, components){
        components['org.opencastproject.workflow.config.trimHold'].setValue(values['org.opencastproject.workflow.config.trimHold']);
        components['org.opencastproject.workflow.config.archiveOp'].setValue(values['org.opencastproject.workflow.config.archiveOp']);
        components['license'].setValue(values['license']);
      }
    </script>
  ]]>
  </configuration_panel>

  <operations>

    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
    <!-- Ingest and prepare                                                -->
    <!--                                                                   -->
    <!-- Download from external ingest nodes, tag, inspect and and prepare -->
    <!-- source recording and metadata catalogs.                           -->
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <!-- Ingest elements from external working file repositories and file servers -->

    <operation
      id="ingest-download"
      description="Ingesting external elements">
      <configurations>
        <configuration key="delete-external">true</configuration>
      </configurations>
    </operation>

    <!-- Inspect the media -->

    <operation
      id="inspect"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Inspecting audio and video streams">
      <configurations>
        <configuration key="overwrite">false</configuration>
        <configuration key="accept-no-media">false</configuration>
      </configurations>
    </operation>

    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
    <!-- Preview, trimming and captions                                    -->
    <!--                                                                   -->
    <!-- Encode audio and video formats necessary for review and trimming. -->
    <!-- Finally, allow for uploading of captions.                         -->
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <!-- encode to hold preview player formats -->

    <operation 
      id="compose"
      if="${trimHold}"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Encoding presenter (camera) video for preview">
      <configurations>
        <configuration key="target-flavor">*/preview</configuration>
        <configuration key="encoding-profile">fast-preview.http</configuration>
        <configuration key="source-flavor">*/source</configuration>
      </configurations>
    </operation>

    <!-- hold for review and trimming --> 

    <operation
      id="trim"
      if="${trimHold}"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Waiting for user to review and trim">
      <configurations>
        <configuration key="encoding-profile">trim.work</configuration>
        <configuration key="target-flavor-subtype">trimmed</configuration>
        <configuration key="source-flavor">*/source</configuration>
      </configurations>
    </operation>


    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
    <!-- Encode for publication to Engage                                  -->
    <!--                                                                   -->
    <!-- Encode audio and video formats to the distribution formats that   -->
    <!-- are required by the Engage publication channel.                   -->
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <!-- encode to engage search result thumbnails -->

    <operation
      id="image"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Creating Engage search result thumbnails">
      <configurations>
        <configuration key="source-flavor">*/trimmed</configuration>
        <configuration key="source-tags"></configuration>
        <configuration key="target-flavor">*/search+preview</configuration>
        <configuration key="target-tags">engage-download</configuration>
        <configuration key="encoding-profile">search-cover.http</configuration>
        <configuration key="time">1</configuration>
      </configurations>
    </operation>

    <!-- encode to engage player preview images -->

    <operation
      id="image"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Creating Engage player preview image">
      <configurations>
        <configuration key="source-flavor">*/trimmed</configuration>
        <configuration key="source-tags"></configuration>
        <configuration key="target-flavor">*/player+preview</configuration>
        <configuration key="target-tags">engage-download</configuration>
        <configuration key="encoding-profile">player-preview.http</configuration>
        <configuration key="time">1</configuration>
      </configurations>
    </operation>

    <!-- encode to feed preview images -->

    <!-- Uncomment this if you need the feed preview image
    <operation
      id="image"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Creating feed preview images">
      <configurations>
        <configuration key="source-flavor">*/trimmed</configuration>
        <configuration key="target-flavor">*/feed+preview</configuration>
        <configuration key="target-tags">rss,atom</configuration>
        <configuration key="encoding-profile">feed-cover.http</configuration>
        <configuration key="time">1</configuration>
      </configurations>
    </operation>
    -->

    <!-- encode for engage player and feeds -->

    <operation
      id="compose"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Encoding for engage player and feeds">
      <configurations>
        <configuration key="source-flavor">*/trimmed</configuration>
        <configuration key="target-flavor">*/delivery</configuration>
        <configuration key="target-tags">engage-download,engage-streaming,rss,atom</configuration>
        <configuration key="encoding-profile">fast.http</configuration>
      </configurations>
    </operation>

    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
    <!-- Segment video streams and extract metadata                        -->
    <!--                                                                   -->
    <!-- Apply the video segmentation algorithm to the presentation tracks -->
    <!-- and extract segment preview images and metadata.                  -->
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <!-- run the videosegmentation -->

    <operation
      id="segment-video"
      fail-on-error="false"
      exception-handler-workflow="error"
      description="Detecting slide transitions in presentation track">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
        <configuration key="target-tags">engage-download</configuration>
      </configurations>
    </operation>

    <!-- Generate segment preview images -->

    <operation
      id="segmentpreviews"
      fail-on-error="false"
      exception-handler-workflow="error"
      description="Creating presentation segments preview image">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
        <configuration key="target-flavor">presentation/segment+preview</configuration>
        <configuration key="reference-flavor">presentation/delivery</configuration>
        <configuration key="reference-tags">engage-download</configuration>
        <configuration key="target-tags">engage-download</configuration>
        <configuration key="encoding-profile">player-slides.http</configuration>
      </configurations>
    </operation>

    <!-- Extract text form slide preview images -->

    <operation
      id="extract-text"
      fail-on-error="false"
      exception-handler-workflow="error"
      description="Extracting text from presentation segments">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
        <configuration key="target-tags">engage-download</configuration>
      </configurations>
    </operation>

    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
    <!-- Publish to publication channels                                   -->
    <!--                                                                   -->
    <!-- Send the encoded material along with the metadata to the          -->
    <!-- publication channels.                                             -->
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <!-- Publish to engage player -->

    <operation
      id="publish-engage"
      max-attempts="2"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Publishing to Engage">
      <configurations>
        <configuration key="download-source-flavors">dublincore/*</configuration>
        <configuration key="download-source-tags">engage-download,atom,rss,mobile</configuration>
        <configuration key="streaming-source-tags">engage-streaming</configuration>
        <configuration key="check-availability">true</configuration>
      </configurations>
    </operation>

    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
    <!-- Archive                                                           -->
    <!--                                                                   -->
    <!-- Store everything that has been tagged accordingly in the archive. -->
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <!-- Archive the current state of the mediapackage -->

    <operation
      if="${archiveOp}"
      id="archive"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Archiving">
      <configurations>
        <configuration key="source-flavors">*/trimmed,dublincore/*,mpeg-7/text</configuration>
      </configurations>
    </operation>

    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
    <!-- Cleanup                                                           -->
    <!--                                                                   -->
    <!-- Remove the recording's artefacts from the workspace and the       -->
    <!-- working file repository.                                          -->
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <!-- Cleanup the working file repository -->

    <operation
      id="cleanup"
      fail-on-error="false"
      description="Cleaning up">
      <configurations>
        <configuration key="preserve-flavors"></configuration>
        <configuration key="delete-external">true</configuration>
      </configurations>
    </operation>

  </operations>

</definition>
